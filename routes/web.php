<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//User Route
Route::get('/','UserCOntroller@index');




//Admin Route
Route::get('/admin','AdminController@index');
Route::get('/admin/dashboard','AdminController@callingDashboard');
Route::post('/admin/dashboard','AdminController@Dashboard');
Route::get('/admin/logout','SuperAdminController@Logout');

//Category Route
Route::get('/admin/allcategory','Category@index');
Route::get('/admin/addcategory','Category@callingAddCategory');
Route::post('/admin/addcategory','Category@addCategory');
Route::get('/admin/allcategory/edit/{id}','Category@editCategory');
Route::get('/admin/allcategory/withhold/{id}','Category@withHold');
Route::get('/admin/allcategory/publish/{id}','Category@publish');
Route::get('/admin/allcategory/delete/{id}','Category@deleteCategory');
Route::post('/admin/allcategory/update/{id}','Category@updateCategory');