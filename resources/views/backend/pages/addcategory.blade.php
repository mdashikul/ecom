@extends('backend.layouts')
@section('breadcam')
    <a href="{{URL::to('/admin/addcategory')}}">Add Category</a>
@endsection
@section('content')
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Category</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <?php
                    $message = Session::get('cat_insert_message');
                    if ($message){
                        echo '<div class="alert alert-error" role="alert">'.$message.'</div>';
                        Session::put('cat_insert_message',null);
                    }
                ?>
                <form class="form-horizontal" action="{{URL::to('/admin/addcategory')}}" method="POST">
                    {{@csrf_field()}}
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Category Name</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="typeahead" name="category_name" required>
                            </div>
                        </div>
                        <div class="control-group hidden-phone">
                            <label class="control-label" for="textarea2">Category Description</label>
                            <div class="controls">
                                <textarea class="cleditor" id="textarea2" rows="3" name="category_desc" required></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="selectError3">Publishing Status</label>
                            <div class="controls">
                                <select id="selectError3" name="publishing_status" required>
                                    <option value="">Select One</option>
                                    <option value="1">Publish</option>
                                    <option value="0">Withhold</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary" id="addcatbtn">Add Category</button>
                            <button type="reset" class="btn">Reset</button>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div><!--/span-->
    </div>
@endsection
@section('js')
    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--// $('#addcatbtn').prop('disabled',true);--}}
            {{--//  $('#textarea2').keyup(function () {--}}
            {{--//      $('#addcatbtn').prop('disabled',false);--}}
            {{--//  });--}}
            {{--// $('#textarea2').change(function () {--}}
            {{--//     $('#addcatbtn').prop('disabled',false);--}}
            {{--// });--}}
        {{--});--}}
    {{--</script>--}}
@endsection