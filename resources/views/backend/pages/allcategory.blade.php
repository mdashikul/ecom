@extends('backend.layouts')
@section('breadcam')
    <a href="{{URL::to('/admin/allcategory')}}">All Category</a>
@endsection
@section('content')
    <?php
    $message = Session::get('cat_insert_message');
    if ($message){
        echo '<div class="alert alert-success" role="alert">'.$message.'</div>';
        Session::put('cat_insert_message',null);
    }
    ?>
    
    <div class="row-fluid sortable">
        <div class="box span12">
            <?php
            $message = Session::get('cat_insert_message');
            if ($message){
                echo '<div class="alert alert-success" role="alert">'.$message.'</div>';
                Session::put('cat_insert_message',null);
            }
            $publish_message = Session::get('publish_message');
            if ($publish_message){
                echo '<div class="alert alert-success" role="alert">'.$publish_message.'</div>';
                Session::put('publish_message',null);
            }
            ?>
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                    <tr>
                        <th>Sl No</th>
                        <th>Category Name</th>
                        <th>Category Description</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i = 0;
                        foreach ($data as $result){
                            $i++;
                    ?>
                        <tr>
                            <td>{{$i}}</td>
                            <td class="center">{{$result->category_name}}</td>
                            <td class="center">{{$result->category_desc}}</td>
                            <td class="center">
                                @if($result->publishing_status === 1)
                                    <span class="label label-success">Publish</span>
                                 @else
                                    <span class="label label-danger">With Hold</span>
                                @endif
                            </td>
                            <td class="center">
                                @if($result->publishing_status === 1)
                                    <a class="btn btn-danger" class="status"  href="{{URL::to('/admin/allcategory/withhold/'.$result->category_id.'')}}">
                                        <i class="halflings-icon white thumbs-down"></i>
                                    </a>
                                @else
                                    <a class="btn btn-success" class="css"  href="{{URL::to('/admin/allcategory/publish/'.$result->category_id.'')}}">
                                        <i class="halflings-icon white thumbs-up"></i>
                                    </a>
                                @endif
                                <a class="btn btn-info" href="{{URL::to('/admin/allcategory/edit/'.$result->category_id.'')}}">
                                    <i class="halflings-icon white edit"></i>
                                </a>
                                <a class="btn btn-danger" id="delete" href="{{URL::to('/admin/allcategory/delete/'.$result->category_id.'')}}">
                                    <i class="halflings-icon white trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php }?>

                    </tbody>
                </table>
            </div>
        </div><!--/span-->
        <style>
            .label-danger{
                background: red;
            }
        </style>
    </div>
@endsection
@section('js')
    <script>
        $(document).on('click','#delete',function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            bootbox.confirm('Are you want to Delete?',function (confirm) {
                if (confirm){
                    window.location.href = link;
                }
            });

        });
        // $(document).on('click','#status',function (e) {
        //     e.preventDefault();
        //     var link = $(this).attr('href');
        //     bootbox.confirm('Are you want to Change Status?',function (confirm) {
        //         if (confirm){
        //             window.location.href = link;
        //         }
        //     });
        //
        // });
    </script>
    <script>
        $(document).ready(function () {
            $('.status').click(function (e) {
                e.preventDefault();
                var links = $(this).attr('href');
                bootbox.confirm('Are you want to change Publishing Status?',function (confirm) {
                    if (confirm){
                        window.location.href = links;
                    }
                });
            });
        });
    </script>
@endsection