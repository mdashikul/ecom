<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

session_start();
class Category extends Controller
{
    public function index(){
        $getcat = DB::table('tbl_category')->get();
        $manageCat = view('backend.pages.allcategory')->with('data',$getcat);
        return view('backend.layouts')->with('data',$manageCat);
    }
    public function callingAddCategory(){
        return view('backend.pages.addcategory');
    }
    public function addCategory(Request $request){
        $data = array();
        $data['category_name'] =$request->category_name;
        $data['category_desc'] =$request->category_desc;
        $data['publishing_status'] =$request->publishing_status;
        $insert =  DB::table('tbl_category')->insert($data);
        if ($insert){
            Session::put('cat_insert_message','Category added successfully...!!!');
            return Redirect::to('/admin/allcategory');
        }else{
            Session::put('cat_insert_message','Category not added');
            return Redirect::to('/admin/addcategory');
        }
    }
     public function  editCategory($id){
       $getCat =  DB::table('tbl_category')->where('category_id',$id)->first();
        $manageCat = view('backend.pages.editcategory')->with('cat',$getCat);
        return view('backend.layouts')->with('cat',$manageCat);
    }
    public function  withHold($id){
        DB::table('tbl_category')->where('category_id',$id)->update(['publishing_status' => 0]);
        Session::put('publish_message','Category Change to With Hold');
        return Redirect::to('/admin/allcategory');
    }
    public function  publish($id){
        DB::table('tbl_category')->where('category_id',$id)->update(['publishing_status' => 1]);
        Session::put('publish_message','Category Change to Publish');
        return Redirect::to('/admin/allcategory');
    }

    public function deleteCategory($id){
        DB::table('tbl_category')->where('category_id',$id)->delete();
        Session::put('publish_message','Category Delete Successfully...!!!!');
        return Redirect::to('/admin/allcategory');
    }

    public function updateCategory($id,Request $request){
        DB::table('tbl_category')->where('category_id',$id)->update([
            'category_name' => $request->category_name,
            'category_desc' => $request->category_desc
        ]);

        Session::put('publish_message','Category Update Successfully..!!!');
        return Redirect::to('admin/allcategory');
    }
}
