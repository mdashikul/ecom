<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
session_start();
class AdminController extends Controller
{
   public function index(){
       if (Session::get('admin_name') && Session::get('admin_id')){

           return Redirect::to('/admin/dashboard');
       }else{
           return view('backend.pages.login');
       }

   }
   public function callingDashboard(){
       if (Session::get('admin_name') && Session::get('admin_id')){
           return view('backend.pages.dasboard');
       }else{
           return Redirect::to('/admin');
       }

   }
   public function Dashboard(Request  $request){
       $email = $request->email;
       $password = $request->password;
        $logdata = DB::table('tbl_admin')->where('admin_email',$email)->where('admin_password',$password)->first();
        if ($logdata){
            Session::put('admin_name',$logdata->admin_name);
            Session::put('admin_id',$logdata->admin_id);

            return Redirect::to('/admin/dashboard');
        }else{
            Session::put('message','Email or Password Invalid...!!!');
            return Redirect::to('/admin');
        }
   }

}
